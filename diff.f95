module diff
    implicit none
    integer, parameter :: mp = 4
    contains

        function f1(x,y); real(mp) f1,x,y; f1=(1 - 2*x)/y**2; end function f1
        function f2(x,y); real(mp) f2,x,y; f2=4*x-2*y; end function f2
        function f3(x,y); real(mp) f3,x,y; f3=cos(x-y); end function f3
        function f4(x,y); real(mp) f4,x,y; f4=exp(x)-y; end function f4
        function f5(x,y); real(mp) f5,x,y; f5=sin(x)-y; end function f5
        function f6(x,y); real(mp) f6,x,y; f6=(1 + y**2)/(1 + x**2); end function f6
        function f7(x,y); real(mp) f7,x,y; f7=2*x-y; end function f7
        function f8(x,y); real(mp) f8,x,y; f8=3*x - 2*y + 5; end function f8
        function f9(x,y); real(mp) f9,x,y; f9=exp(2*x) - 1; end function f9
        function f10(x,y); real(mp) f10,x,y; f10=-2*y/(y**2 - 6*x); end function f10


       
        function Euler(f, n, h, x, y) 
            real(mp), allocatable :: x1(:), y1(:), Euler(:)
            real(mp)  h, x, y
            real(mp), external :: f
            integer(mp) i,n 
                        
            allocate(x1(1:n + 1), y1(1:n + 1), Euler(1:n+1))

            x1(1) = x
            y1(1) = y

            do i = 1, n
                y1(i+1) = y1(i) + h*f(x1(i), y1(i))
                x1(i+1) = x1(i) + h
            enddo

            Euler = y1
            
        
        end function Euler
    
        function Runge_kutta(f, n, h, x, y) 
            real(mp), allocatable :: x1(:), y1(:), Runge_kutta(:)
            real(mp)  h, y, x, k1, k2, k3, k4
            real(mp), external :: f
            integer(mp) i,n 

            allocate(x1(0:n), y1(0:n), Runge_kutta(0:n))

            x1(0) = x
            y1(0) = y

            do i = 0, n 
                k1 = f(x1(i), y1(i))
                k2 = f(x1(i) + h/2, y1(i) + h/2 * k1)
                k3 = f(x1(i) + h/2, y1(i) + h/2 * k2)
                k4 = f(x1(i) + h, y1(i) + h*k3)

                y1(i+1) = y1(i) + h/6 * (k1 + 2*k2 + 2*k3 + k4)
                x1(i+1) = x1(i) + h
            enddo
            
            
           Runge_kutta = y1


        end function Runge_kutta


        function Adams(f, n, h, x, y)
            real(mp), allocatable :: x1(:), y1(:), Adams(:)
            real(mp)  h, y, x
            real(mp), external :: f
            integer i,n 

            allocate(x1(1:n + 4), y1(1:n + 4), Adams(1:n + 4))

            x1(1) = x
            y1(1) = y

            do i = 1, n
                y1(i+1) = y1(i) + h * f(x1(i), y1(i))
                x1(i+1) = x1(i) + h
            enddo

            y1(n + 2) = y1(n + 1) + h*(3/2 * f(x1(n+1), y1(n+1)) - 1/2 * f(x1(n), y1(n)))
            
            y1(n + 3) = y1(n + 2) + h*(23/12 * f(x1(n+2), y1(n+2)) - &
            4/3 * f(x1(n+1), y1(n+1)) + 5/12 * f(x1(n), y1(n)))
            
            y1(n + 4) = y1(n + 3) + h*(55/24 * f(x1(n+3), y1(n+3)) - &
             59/24 * f(x1(n+2), y1(n+2)) + 37/24 * f(x1(n+1), y1(n+1)) - 3/8 * f(x1(n), y1(n)))


            Adams = y1

        end function Adams

            

    
    end module diff