com=gfortran
ras=f95
files=$(wildcard *.$(ras))
main : $(patsubst %.$(ras), %.o, $(files))
	 $(com) $^ -o $@
%.o : %.$(ras) diff.mod
	 $(com) -c $<
diff.mod : diff.f95
	 $(com) -c $<
result : main input
	 ./main < input > result
clean :
	 rm -f *.exe; rm -f main.o
